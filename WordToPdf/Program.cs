﻿using System;
using Word = Microsoft.Office.Interop.Word;

namespace WordToPdf
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                System.Console.WriteLine("Usage: cscript " +
                    System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName +
                    " {inputDocName} {outputDocName}");
            }
            else
            {
                Word.Application wdApp = null;
                Word.Document doc = null;
                try
                {
                    wdApp = new Word.Application();

                    doc = wdApp.Documents.Open(args[0], ReadOnly: true);
                    doc.ExportAsFixedFormat(args[1], Word.WdExportFormat.wdExportFormatPDF);
                    doc.Close(false);

                    wdApp.Quit();
                }
                finally
                {
                    //Clean Up
                    releaseObject(wdApp);
                    releaseObject(doc);
                }
            }
        }

        private static void releaseObject(object obj)
        {
            if (obj == null)
            {
                return;
            }
            else
            {
                try
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                    obj = null;
                }
                catch (Exception)
                {
                    obj = null;
                }
                finally
                {
                    GC.Collect();
                }
            }
        }
    }
}
